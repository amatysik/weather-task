# WeatherTask

Being most proficient in Angular, I decided to use it to have more time to focus on application design rather than framework-fu.
My main focus for the task was to showcase use of reactive components that connect to an external data store. Points to note:

-   The store implementation and structure is hidden from the components.
-   In this case simple services are used to keep the state, but any popular store could be used, i.e. NGRX
-   State connectors are used to allow components to consume state and dispatch events.
-   State connectors are testable and provide data in a form relevant to the component, merging data from different state slices if necessary

I ran out of time anyway, so let me describe my approach and plans:

My plan for consuming the API was to keep weather per location cached in the store, and update on an interval or user request.

-   Dashboard is requesting weather for all locations to display the temperatures.
-   Opening the details screen would not require additional data fetching
-   The details screen would have its own state connector that would provide the full weather information if found in the store
-   If the weather was not found, the details connector would trigger a fetch for that one specific locatoin

My plan for adding locations:

-   Adding a location would be triggered by the Dashboard via its existing state connector
-   Locations would be stored in a separate service - Dashboard would connect via its state connector, allowing it to observe locations and order weather fetches for new ones (for example, after a new on is added)
-   The locations service would use Local Storage to persist user locations and load them back between sessions.

My plan for error handling:

-   any errors with the API connection should not stop the app, they would be displayed in a non-stopping manner and empty values would be presented (i.e. "--") presented with an option to attempt the weather refresh to try again

My plan for displaying data:

-   use pipes to convert units
-   use flexbox to keep the layout responsive while avoiding media queries

The remaining list of my orignal TODO's is at the bottom of this readme and also found in relevant places around the code.

I hope you have a nice read :)

Thanks,

Adam

## Development setup

1. Please add your own API key by creating `weather-api-key.js` in the root folder (an example file is available)
2. Make sure you have `Node.js` installed with `npm`
3. Run `npm install`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Deployment setup

Make sure that the deployment environment:

-   has an api key file present `weather-api-key.js` (and placed as a sibling of `index.html`)
-   has rewrite enabled and is rewriting all non-file paths to `index.html`

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.2.

# TODO

-   Create a mobile-first responsive UI layout
-   Handle connection errors
-   Map Kelvin temperatures to Celsius/Fahrenheit
-   Add loading indicators for Dashboard temperatures
-   Display weather information on the Details page
-   Allow adding "My location"
-   Persist "My location" in storage
-   Allow editing of "My location"
