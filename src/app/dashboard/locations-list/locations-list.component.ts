import { Component, Input, OnInit } from "@angular/core";
import { LocationListItem } from "../definitions";

@Component({
    selector: "app-locations-list",
    templateUrl: "./locations-list.component.html",
    styleUrls: ["./locations-list.component.scss"],
})
export class LocationsListComponent implements OnInit {
    @Input() locations: LocationListItem[] | null = [];

    constructor() {}

    ngOnInit(): void {}
}
