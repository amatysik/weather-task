import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DashboardComponent } from "./dashboard.component";
import { CommonUiModule } from "../common-ui/common-ui.module";
import { LocationsListComponent } from "./locations-list/locations-list.component";
import { RouterModule } from "@angular/router";

@NgModule({
    declarations: [DashboardComponent, LocationsListComponent],
    imports: [CommonModule, CommonUiModule, RouterModule],
})
export class DashboardModule {}
