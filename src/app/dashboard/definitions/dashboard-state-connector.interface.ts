import { Observable } from "rxjs";
import { LocationListItem } from "./location-list-item.interface";

export interface DashboardStateConnector {
    locationsList$: Observable<LocationListItem[]>;
    addLocation(locationName: string): void;
}
