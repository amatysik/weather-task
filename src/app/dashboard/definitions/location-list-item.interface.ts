export interface LocationListItem {
    locationName: string;
    currentTemperature: number | null;
}
