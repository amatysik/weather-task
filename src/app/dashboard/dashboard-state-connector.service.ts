import { Injectable } from "@angular/core";
import { DashboardStateConnector } from "./definitions/dashboard-state-connector.interface";
import { WeatherStateService } from "../state/weather-state.service";
import { map } from "rxjs/operators";
import { LocationListItem } from "./definitions";

@Injectable()
export class DashboardStateConnectorService implements DashboardStateConnector {
    constructor(private weatherStateService: WeatherStateService) {}

    locationsList$ = this.weatherStateService.weatherState$.pipe(
        map((weatherState) => {
            return Object.keys(weatherState).map<LocationListItem>((locationName) => ({
                locationName,
                currentTemperature: weatherState[locationName].weather?.main.temp || null,
            }));
        })
    );

    addLocation(locationName: string): void {
        this.weatherStateService.updateWeatherForLocation(locationName);
    }
}
