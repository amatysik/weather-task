import { TestBed } from "@angular/core/testing";
import { DashboardStateConnectorService } from "./dashboard-state-connector.service";
import { WeatherStateService } from "../state/weather-state.service";
import { WeatherState } from "../state/definitions";
import { LocationListItem } from "./definitions";
import { weatherResponseMock } from "../api/weather/mocks/weather-response.mock";
import { TestScheduler } from "rxjs/testing";
import { RunHelpers } from "rxjs/internal/testing/TestScheduler";

describe("DashboardStateConnectorService", () => {
    let service: DashboardStateConnectorService;
    let weatherStateServiceMock: any;

    weatherStateServiceMock = jasmine.createSpy("WeatherStateService");

    const testScheduler = new TestScheduler((actual, expected) => {
        // asserting the two objects are equal
        expect(actual).toEqual(expected);
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [{ provide: WeatherStateService, useValue: weatherStateServiceMock }],
        });
    });

    it("should emit a list of locations with temperatures on each weather update", () => {
        const LOCATION_NAME = "test location";

        const pendingWeatherState: WeatherState = {
            [LOCATION_NAME]: { loading: true, weather: null },
        };

        const loadedWeatherState: WeatherState = {
            [LOCATION_NAME]: { loading: false, weather: weatherResponseMock },
        };

        const expectedNoTempLIst: LocationListItem[] = [
            {
                locationName: LOCATION_NAME,
                currentTemperature: null,
            },
        ];

        const expectedListWithTemps: LocationListItem[] = [
            {
                locationName: LOCATION_NAME,
                currentTemperature: 282.55,
            },
        ];

        testScheduler.run((helpers: RunHelpers) => {
            const { expectObservable } = helpers;

            weatherStateServiceMock.weatherState$ = testScheduler.createColdObservable("e-p-l|", {
                e: {},
                p: pendingWeatherState,
                l: loadedWeatherState,
            });
            service = new DashboardStateConnectorService(weatherStateServiceMock);

            const expected = "e-p-l|";
            const expectedValues = {
                e: [],
                p: expectedNoTempLIst,
                l: expectedListWithTemps,
            };

            const result = service.locationsList$;
            expectObservable(result).toBe(expected, expectedValues);
        });
    });
});
