import { Component, OnInit } from "@angular/core";
import { DashboardStateConnectorService } from "./dashboard-state-connector.service";

@Component({
    selector: "app-dashboard",
    templateUrl: "./dashboard.component.html",
    styleUrls: ["./dashboard.component.scss"],
    providers: [DashboardStateConnectorService],
})
export class DashboardComponent implements OnInit {
    constructor(public dashboardStateConnector: DashboardStateConnectorService) {}

    ngOnInit(): void {
        this.dashboardStateConnector.addLocation("London");
        this.dashboardStateConnector.addLocation("Berlin");
    }
}
