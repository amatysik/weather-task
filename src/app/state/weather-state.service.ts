import { Injectable } from "@angular/core";
import { BehaviorSubject, of } from "rxjs";
import { WeatherApiService } from "../api/weather/weather-api.service";
import { catchError } from "rxjs/operators";
import { WeatherState } from "./definitions";

@Injectable({
    providedIn: "root",
})
export class WeatherStateService {
    private weatherStateSubject = new BehaviorSubject<WeatherState>({});
    weatherState$ = this.weatherStateSubject.asObservable();

    constructor(private weatherApiService: WeatherApiService) {}

    updateWeatherForLocation(locationName: string): void {
        this.eimtUpdatedWeatherState({
            [locationName]: { loading: true, weather: null },
        });
        this.weatherApiService
            .getWeatherByLocation(locationName)
            .pipe(catchError(() => of(null))) // TODO present the error message to the user
            .subscribe((weather) => {
                this.eimtUpdatedWeatherState({
                    [locationName]: { loading: false, weather },
                });
            });
    }

    private eimtUpdatedWeatherState(state: WeatherState): void {
        this.weatherStateSubject.next({
            ...this.weatherStateSubject.getValue(),
            ...state,
        });
    }
}
