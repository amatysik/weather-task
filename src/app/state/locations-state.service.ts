import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({
    providedIn: "root",
})
export class LocationsStateService {
    private locationsSubject = new BehaviorSubject<string[]>([]);
    public locations$ = this.locationsSubject.asObservable();

    addLocation(locationName: string) {
        this.locationsSubject.next([...this.locationsSubject.getValue(), locationName]);
    }

    // TODO save to storage, load initial state
}
