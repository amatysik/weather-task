import { Weather } from "../../api/weather/definitions";

export interface WeatherState {
    [locationName: string]: {
        loading: boolean;
        weather: Weather | null;
    };
}
