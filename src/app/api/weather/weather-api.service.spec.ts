import { TestBed } from "@angular/core/testing";

import { WeatherApiService } from "./weather-api.service";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import { weatherResponseMock } from "./mocks/weather-response.mock";

describe("WeatherApiService", () => {
    let httpTestingController: HttpTestingController;
    let service: WeatherApiService;

    beforeEach(() => {
        TestBed.configureTestingModule({ imports: [HttpClientTestingModule] });
        service = TestBed.inject(WeatherApiService);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it("should be created", () => {
        expect(service).toBeTruthy();
    });

    describe("getWeatherByLocation", () => {
        afterEach(() => {
            delete (window as any).weatherApiKey;
        });

        it("should throw an error if key to the API is not found", () => {
            const locationName = "TEST_LOCATION";
            const response = "response";

            expect(() => {
                service.getWeatherByLocation(locationName).subscribe((weather) => {
                    expect(weather).toEqual(weatherResponseMock);
                });
                httpTestingController.expectOne(service.url);
            }).toThrow();
        });

        it("should call the API with correct params", () => {
            const locationName = "TEST_LOCATION";
            const apiKey = "abc";

            (window as any).weatherApiKey = apiKey;
            service.getWeatherByLocation(locationName).subscribe((mock) => {
                expect(mock).toEqual(weatherResponseMock);
            });

            const request = httpTestingController.expectOne(
                service.url + `?appid=${apiKey}&q=${locationName}`
            );
            expect(request.request.method).toEqual("GET");
            request.flush(weatherResponseMock);
        });
    });
});
