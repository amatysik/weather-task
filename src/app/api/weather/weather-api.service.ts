import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Weather } from "./definitions";

@Injectable({
    providedIn: "root",
})
export class WeatherApiService {
    public readonly url = "https://api.openweathermap.org/data/2.5/weather";

    private get apiKey(): string {
        const key = (window as any).weatherApiKey;
        if (!key)
            throw new Error(
                "Key to the weather API not found. Please follow instructions in README.md"
            );
        return key as string;
    }

    constructor(private http: HttpClient) {}

    getWeatherByLocation(locationName: string): Observable<Weather> {
        return this.http.get<Weather>(this.url, {
            params: {
                appid: this.apiKey,
                q: locationName,
            },
        });
    }
}
