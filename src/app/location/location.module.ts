import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LocationComponent } from "./location.component";
import { CommonUiModule } from "../common-ui/common-ui.module";
import { RouterModule } from "@angular/router";

@NgModule({
    declarations: [LocationComponent],
    imports: [CommonModule, CommonUiModule, RouterModule],
    exports: [LocationComponent],
})
export class LocationModule {}
