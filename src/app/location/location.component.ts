import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { LOCATION_NAME_ROUTE_PARAM } from "./definitions";

@Component({
    selector: "app-location",
    templateUrl: "./location.component.html",
    styleUrls: ["./location.component.scss"],
})
export class LocationComponent implements OnInit {
    public currentLocationName: string = "";

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit(): void {
        this.currentLocationName = this.activatedRoute.snapshot.params[LOCATION_NAME_ROUTE_PARAM];
    }
}
