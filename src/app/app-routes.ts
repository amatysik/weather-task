import { Routes } from "@angular/router";
import { NotFoundComponent } from "./not-found/not-found.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { LocationComponent } from "./location/location.component";
import { LOCATION_NAME_ROUTE_PARAM } from "./location/definitions";

export const routes: Routes = [
    {
        path: "",
        component: DashboardComponent,
    },
    {
        path: `location/:${LOCATION_NAME_ROUTE_PARAM}`,
        component: LocationComponent,
    },
    {
        path: "**",
        component: NotFoundComponent,
    },
];
