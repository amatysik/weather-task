import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppComponent } from "./app.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { AppRoutingModule } from "./app-routing.module";
import { DashboardModule } from "./dashboard/dashboard.module";
import { LocationModule } from "./location/location.module";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
    declarations: [AppComponent, NotFoundComponent],
    imports: [BrowserModule, AppRoutingModule, DashboardModule, LocationModule, HttpClientModule],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
